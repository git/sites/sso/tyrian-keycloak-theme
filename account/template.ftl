<#macro mainLayout active bodyClass>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow">

    <link href="https://assets.gentoo.org/tyrian/v2/tyrian.min.css" rel="stylesheet" media="screen">

    <title>${msg("accountManagementTitle")}</title>
    <link rel="icon" href="${url.resourcesPath}/img/favicon.ico">
    <#if properties.styles?has_content>
        <#list properties.styles?split(' ') as style>
            <link href="${url.resourcesPath}/${style}" rel="stylesheet" />
        </#list>
    </#if>
    <#if properties.scripts?has_content>
        <#list properties.scripts?split(' ') as script>
            <script type="text/javascript" src="${url.resourcesPath}/${script}"></script>
        </#list>
    </#if>
</head>
<body class="admin-console user ${bodyClass}">

<header>
    <div class="site-title">
        <div class="container">
            <div class="row justify-content-between">
                <div class="logo">
                    <a href="/" title="Back to the homepage" class="site-logo">
                        <img src="https://assets.gentoo.org/tyrian/v2/site-logo.png" alt="Gentoo" srcset="https://assets.gentoo.org/tyrian/v2/site-logo.svg">
                    </a>
                    <span class="site-label">SSO</span>
                </div>
                <div class="site-title-buttons">
                    <div class="btn-group btn-group-sm">
                        <a href="https://get.gentoo.org/" role="button" class="btn get-gentoo"><span class="fa fa-fw fa-download"></span>
                            <strong>Get Gentoo!</strong></a>
                        <div class="btn-group btn-group-sm">
                            <a class="btn gentoo-org-sites dropdown-toggle" data-toggle="dropdown" data-target="#" href="#">
                                <span class="fa fa-fw fa-map-o"></span> <span class="d-none d-sm-inline">gentoo.org sites</span>
                                <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="https://www.gentoo.org/" title="Main Gentoo website"><span class="fa fa-home fa-fw"></span>
                                    gentoo.org</a>
                                <a class="dropdown-item" href="https://wiki.gentoo.org/" title="Find and contribute documentation"><span class="fa fa-file-text-o fa-fw"></span>
                                    Wiki</a>
                                <a class="dropdown-item" href="https://bugs.gentoo.org/" title="Report issues and find common issues"><span class="fa fa-bug fa-fw"></span>
                                    Bugs</a>
                                <a class="dropdown-item" href="https://forums.gentoo.org/" title="Discuss with the community"><span class="fa fa-comments-o fa-fw"></span>
                                    Forums</a>
                                <a class="dropdown-item" href="https://packages.gentoo.org/" title="Find software for your Gentoo"><span class="fa fa-hdd-o fa-fw"></span>
                                    Packages</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="https://planet.gentoo.org/" title="Find out what's going on in the developer community"><span class="fa fa-rss fa-fw"></span>
                                    Planet</a>
                                <a class="dropdown-item" href="https://archives.gentoo.org/" title="Read up on past discussions"><span class="fa fa-archive fa-fw"></span>
                                    Archives</a>
                                <a class="dropdown-item" href="https://sources.gentoo.org/" title="Browse our source code"><span class="fa fa-code fa-fw"></span>
                                    Sources</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="https://infra-status.gentoo.org/" title="Get updates on the services provided by Gentoo"><span class="fa fa-server fa-fw"></span>
                                    Infra Status</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav class="tyrian-navbar navbar navbar-dark navbar-expand-lg bg-primary" role="navigation">
        <div class="container px-0">
            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-main-collapse" aria-controls="navbar-main-collapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse navbar-main-collapse" id="navbar-main-collapse">
                <ul class="navbar-nav ml-3 mr-auto">
                    <li class="nav-item <#if active=='account'>active</#if>"><a class="nav-link" href="${url.accountUrl}">${msg("account")}</a></li>
                    <#if features.passwordUpdateSupported><li class="nav-item <#if active=='password'>active</#if>"><a class="nav-link" href="${url.passwordUrl}">${msg("password")}</a></li></#if>
                    <li class="nav-item <#if active=='totp'>active</#if>"><a class="nav-link" href="${url.totpUrl}">${msg("authenticator")}</a></li>
                    <#if features.identityFederation><li class="nav-item <#if active=='social'>active</#if>"><a class="nav-link" href="${url.socialUrl}">${msg("federatedIdentity")}</a></li></#if>
                    <li class="nav-item <#if active=='sessions'>active</#if>"><a class="nav-link" href="${url.sessionsUrl}">${msg("sessions")}</a></li>
                    <li class="nav-item <#if active=='applications'>active</#if>"><a class="nav-link" href="${url.applicationsUrl}">${msg("applications")}</a></li>
                    <#if features.log><li class="nav-item <#if active=='log'>active</#if>"><a class="nav-link" href="${url.logUrl}">${msg("log")}</a></li></#if>
                    <#if realm.userManagedAccessAllowed && features.authorization><li class="nav-item <#if active=='authorization'>active</#if>"><a class="nav-link" href="${url.resourceUrl}">${msg("myResources")}</a></li></#if>
                </ul>

                <ul class="navbar-nav ml-auto">

                    <#if realm.internationalizationEnabled>
                    <li>
                        <div class="kc-dropdown" id="kc-locale-dropdown">
                            <a href="#" id="kc-current-locale-link">${locale.current}</a>
                            <ul>
                                <#list locale.supported as l>
                                    <li class="kc-dropdown-item"><a href="${l.url}">${l.label}</a></li>
                                </#list>
                            </ul>
                        </div>
                    <li>
                        </#if>
                    <#if referrer?has_content && referrer.url?has_content><li><a href="${referrer.url}" id="referrer">${msg("backTo",referrer.name)}</a></li></#if>
                    <li class="nav-item"><a class="nav-link" href="${url.logoutUrl}"><i class="fa fa-sign-out" aria-hidden="true"></i>  ${msg("doSignOut")}</a></li>
                </ul>

            </div>
        </div>
    </nav>
</header>


    <div class="container py-4">
        <div class="row">
            <div class="col-3 d-none">
                <ul class="nav flex-column nav-pills">
                    <li class="nav-item"><a class="nav-link <#if active=='account'>active</#if>" href="${url.accountUrl}">${msg("account")}</a></li>
                    <#if features.passwordUpdateSupported><li class="nav-item"><a class="nav-link <#if active=='password'>active</#if>" href="${url.passwordUrl}">${msg("password")}</a></li></#if>
                    <li class="nav-item"><a class="nav-link <#if active=='totp'>active</#if>" href="${url.totpUrl}">${msg("authenticator")}</a></li>
                    <#if features.identityFederation><li class="nav-item"><a class="nav-link <#if active=='social'>active</#if>" href="${url.socialUrl}">${msg("federatedIdentity")}</a></li></#if>
                    <li class="nav-item"><a class="nav-link <#if active=='sessions'>active</#if>" href="${url.sessionsUrl}">${msg("sessions")}</a></li>
                    <li class="nav-item"><a class="nav-link <#if active=='applications'>active</#if>" href="${url.applicationsUrl}">${msg("applications")}</a></li>
                    <#if features.log><li class="nav-item"><a class="nav-link <#if active=='log'>active</#if>" href="${url.logUrl}">${msg("log")}</a></li></#if>
                    <#if realm.userManagedAccessAllowed && features.authorization><li class="nav-item"><a class="nav-link <#if active=='authorization'>active</#if>" href="${url.resourceUrl}">${msg("myResources")}</a></li></#if>
                </ul>
            </div>


            <div class="col-12 content-area">
                <#if message?has_content>
                    <div class="alert alert-${message.type}">
                        <#if message.type=='success' ><span class="pficon pficon-ok"></span></#if>
                        <#if message.type=='error' ><span class="pficon pficon-error-circle-o"></span></#if>
                        <span class="kc-feedback-text">${kcSanitize(message.summary)?no_esc}</span>
                    </div>
                </#if>

                <#nested "content">
            </div>
        </div>
    </div>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-12 offset-md-2 col-md-7">
                    &nbsp;
                </div>
                <div class="col-xs-12 col-md-3">
                    <h3 class="footerhead">Questions or comments?</h3>
                    Please feel free to <a href="https://www.gentoo.org/inside-gentoo/contact/">contact us</a>.
                </div>
            </div>
            <div class="row">
                <div class="col-3 col-md-2">
                    <ul class="footerlinks three-icons">
                        <li><a href="https://twitter.com/gentoo" title="@Gentoo on Twitter"><span class="fa fa-twitter fa-fw"></span></a></li>
                        <li><a href="https://www.facebook.com/gentoo.org" title="Gentoo on Facebook"><span class="fa fa-facebook fa-fw"></span></a></li>
                        <li></li>
                    </ul>
                </div>
                <div class="col-xs-10 col-sm-9 col-md-10">
                    <strong>&copy; 2001&ndash;2020 Gentoo Foundation, Inc.</strong><br>
                    <small>
                        Gentoo is a trademark of the Gentoo Foundation, Inc.
                        The contents of this document, unless otherwise expressly stated, are licensed under the
                        <a href="https://creativecommons.org/licenses/by-sa/4.0/" rel="license">CC-BY-SA-4.0</a> license.
                        The <a href="https://www.gentoo.org/inside-gentoo/foundation/name-logo-guidelines.html">Gentoo Name and Logo Usage Guidelines</a> apply.
                    </small>
                </div>
            </div>
        </div>
    </footer>

<script src="https://assets.gentoo.org/tyrian/v2/jquery-3.3.slim.js"></script>
<script src="https://assets.gentoo.org/tyrian/v2/popper.min.js"></script>
<script src="https://assets.gentoo.org/tyrian/v2/bootstrap.min.js"></script>


</body>
</html>
</#macro>