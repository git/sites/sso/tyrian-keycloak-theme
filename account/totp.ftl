<#import "template.ftl" as layout>
<@layout.mainLayout active='totp' bodyClass='totp'; section>

    <div class="row">
        <div class="col-md-10">
            <h1 class="first-header">${msg("authenticatorTitle")}</h1>
        </div>
        <#if totp.otpCredentials?size == 0>
            <div class="col-md-2 subtitle">
                <span class="subtitle"><span class="required">*</span> ${msg("requiredFields")}</span>
            </div>
        </#if>
    </div>

    <#if totp.enabled>
        <table class="table table-bordered table-striped">
            <thead>
            <#if totp.otpCredentials?size gt 1>
                <tr>
                    <th colspan="4">${msg("configureAuthenticators")}</th>
                </tr>
            <#else>
                <tr>
                    <th colspan="3">${msg("configureAuthenticators")}</th>
                </tr>
            </#if>
            </thead>
            <tbody>
            <#list totp.otpCredentials as credential>
                <tr>
                    <td class="provider">${msg("mobile")}</td>
                    <#if totp.otpCredentials?size gt 1>
                        <td class="provider">${credential.id}</td>
                    </#if>
                    <td class="provider">${credential.userLabel!}</td>
                    <td class="action">
                        <form action="${url.totpUrl}" method="post" class="form-inline">
                            <input type="hidden" id="stateChecker" name="stateChecker" value="${stateChecker}">
                            <input type="hidden" id="submitAction" name="submitAction" value="Delete">
                            <input type="hidden" id="credentialId" name="credentialId" value="${credential.id}">
                            <button id="remove-mobile" class="btn btn-default">
                                <i class="pficon pficon-delete"></i>
                            </button>
                        </form>
                    </td>
                </tr>
            </#list>
            </tbody>
        </table>
        <#else>

    <hr/>

    <div class="d-flex justify-content-center flex-row">

        <div <#if mode?? && mode = "manual">class="d-flex align-items-center"</#if>>
            <#if mode?? && mode = "manual">
                <p><span id="kc-totp-secret-key">${totp.totpSecretEncoded}</span></p>
            <#else>
                <p><img id="qr-code" src="data:image/png;base64, ${totp.totpSecretQrCode}" alt="Figure: Barcode"></p>
            </#if>
        </div>

        <div class="">
            <ol>
                <li>
                    <p>${msg("totpStep1")}</p>

                    <ul>
                        <#list totp.policy.supportedApplications as app>
                            <li>${app}</li>
                        </#list>
                    </ul>
                </li>

                <#if mode?? && mode = "manual">
                    <li>
                        <p>${msg("totpManualStep2")}</p>
                        <p><a href="${totp.qrUrl}" id="mode-barcode">${msg("totpScanBarcode")}</a></p>
                    </li>
                    <li>
                        <p>${msg("totpManualStep3")}</p>
                        <ul>
                            <li id="kc-totp-type">${msg("totpType")}: ${msg("totp." + totp.policy.type)}</li>
                            <li id="kc-totp-algorithm">${msg("totpAlgorithm")}: ${totp.policy.getAlgorithmKey()}</li>
                            <li id="kc-totp-digits">${msg("totpDigits")}: ${totp.policy.digits}</li>
                            <#if totp.policy.type = "totp">
                                <li id="kc-totp-period">${msg("totpInterval")}: ${totp.policy.period}</li>
                            <#elseif totp.policy.type = "hotp">
                                <li id="kc-totp-counter">${msg("totpCounter")}: ${totp.policy.initialCounter}</li>
                            </#if>
                        </ul>
                    </li>
                <#else>
                    <li>
                        <p>${msg("totpStep2")}</p>
                        <p><a href="${totp.manualUrl}" id="mode-manual">${msg("totpUnableToScan")}</a></p>
                    </li>
                </#if>
                <li>
                    <p>${msg("totpStep3")}</p>
                    <p>${msg("totpStep3DeviceName")}</p>
                </li>
            </ol>
        </div>

    </div>

    <hr/>

    <form action="${url.totpUrl}" class="form-horizontal" method="post">
        <input type="hidden" id="stateChecker" name="stateChecker" value="${stateChecker}">
        <div class="form-group row">
            <div class="col-2 col-md-2">
                <label for="totp" class="control-label">${msg("authenticatorCode")}</label> <span class="required">*</span>
            </div>

            <div class="col-10 col-md-10">
                <input type="text" class="form-control" id="totp" name="totp" autocomplete="off" autofocus>
                <input type="hidden" id="totpSecret" name="totpSecret" value="${totp.totpSecret}"/>
            </div>


        </div>

        <div class="form-group row" ${messagesPerField.printIfExists('userLabel',properties.kcFormGroupErrorClass!)}">
            <div class="col-2 col-md-2">
                <label for="userLabel" class="control-label">${msg("totpDeviceName")}</label> <#if totp.otpCredentials?size gte 1><span class="required">*</span></#if>
            </div>

            <div class="col-10 col-md-10">
                <input type="text" class="form-control" id="userLabel" name="userLabel" autocomplete="off">
            </div>
        </div>

        <div class="form-group">
            <div id="kc-form-buttons" class="offset-md-2 col-md-10 px-0 submit">
                <div class="">
                    <button type="submit"
                            class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}"
                            id="saveTOTPBtn" name="submitAction" value="Save">${msg("doSave")}
                    </button>
                    <button type="submit"
                            class="${properties.kcButtonClass!} ${properties.kcButtonDefaultClass!} ${properties.kcButtonLargeClass!}"
                            id="cancelTOTPBtn" name="submitAction" value="Cancel">${msg("doCancel")}
                    </button>
                </div>
            </div>
        </div>
    </form>
    </#if>

    <script>
        function recolorImage(r, g, b, r2, g2, b2) {
            var img1 = document.getElementById("qr-code");
            var img = new Image();
            img.width = img1.width;
            img.height = img1.height;
            img.src = img1.src;
            var c = document.createElement('canvas');
            var ctx = c.getContext("2d");
            var w = img.width;
            var h = img.height;

            c.width = w;
            c.height = h;

            ctx.drawImage(img, 0, 0, w, h);
            var imageData = ctx.getImageData(0, 0, w, h);

            for (var i = 0; i < imageData.data.length; i += 4) {
                if (imageData.data[i] == r && imageData.data[i + 1] == g && imageData.data[i + 2] == b) {
                    imageData.data[i] = r2;
                    imageData.data[i + 1] = g2;
                    imageData.data[i + 2] = b2;
                }
            }

            ctx.putImageData(imageData, 0, 0);
            img1.src = c.toDataURL('image/png');
        }

        window.addEventListener('load', (event) => {
            recolorImage(255, 255, 255, 249, 249, 249);
        });

    </script>

</@layout.mainLayout>
