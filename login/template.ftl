<#macro registrationLayout bodyClass="" displayInfo=false displayMessage=true displayRequiredFields=false displayWide=false showAnotherWayIfPresent=true>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" class="${properties.kcHtmlClass!}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://assets.gentoo.org/tyrian/v2/tyrian.min.css" rel="stylesheet" media="screen">
    <link rel="icon" href="https://www.gentoo.org/favicon.ico" type="image/x-icon">


    <#if properties.meta?has_content>
        <#list properties.meta?split(' ') as meta>
            <meta name="${meta?split('==')[0]}" content="${meta?split('==')[1]}"/>
        </#list>
    </#if>
    <title>Gentoo Single Sign On - ${msg("loginTitle",(realm.displayName!''))}</title>
    <#if properties.styles?has_content>
        <#list properties.styles?split(' ') as style>
            <link href="${url.resourcesPath}/${style}" rel="stylesheet" />
        </#list>
    </#if>
    <#if properties.scripts?has_content>
        <#list properties.scripts?split(' ') as script>
            <script src="${url.resourcesPath}/${script}" type="text/javascript"></script>
        </#list>
    </#if>
    <#if scripts??>
        <#list scripts as script>
            <script src="${script}" type="text/javascript"></script>
        </#list>
    </#if>
</head>

<body class="${properties.kcBodyClass!}">

<header>
      <div class="site-title">
        <div class="container">
          <div class="row justify-content-between">
            <div class="logo">
              <a href="/" title="Back to the homepage" class="site-logo">
                  <img src="https://assets.gentoo.org/tyrian/v2/site-logo.png" alt="Gentoo" srcset="https://assets.gentoo.org/tyrian/v2/site-logo.svg">
              </a>
              <span class="site-label">SSO</span>
            </div>
            <div class="site-title-buttons">
              <div class="btn-group btn-group-sm">
                <a href="https://get.gentoo.org/" role="button" class="btn get-gentoo"><span class="fa fa-fw fa-download"></span>
                  <strong>Get Gentoo!</strong></a>
                <div class="btn-group btn-group-sm">
                  <a class="btn gentoo-org-sites dropdown-toggle" data-toggle="dropdown" data-target="#" href="#">
                    <span class="fa fa-fw fa-map-o"></span> <span class="d-none d-sm-inline">gentoo.org sites</span>
                    <span class="caret"></span>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="https://www.gentoo.org/" title="Main Gentoo website"><span class="fa fa-home fa-fw"></span>
                      gentoo.org</a>
                    <a class="dropdown-item" href="https://wiki.gentoo.org/" title="Find and contribute documentation"><span class="fa fa-file-text-o fa-fw"></span>
                      Wiki</a>
                    <a class="dropdown-item" href="https://bugs.gentoo.org/" title="Report issues and find common issues"><span class="fa fa-bug fa-fw"></span>
                      Bugs</a>
                    <a class="dropdown-item" href="https://forums.gentoo.org/" title="Discuss with the community"><span class="fa fa-comments-o fa-fw"></span>
                      Forums</a>
                    <a class="dropdown-item" href="https://packages.gentoo.org/" title="Find software for your Gentoo"><span class="fa fa-hdd-o fa-fw"></span>
                      Packages</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="https://planet.gentoo.org/" title="Find out what's going on in the developer community"><span class="fa fa-rss fa-fw"></span>
                      Planet</a>
                    <a class="dropdown-item" href="https://archives.gentoo.org/" title="Read up on past discussions"><span class="fa fa-archive fa-fw"></span>
                      Archives</a>
                    <a class="dropdown-item" href="https://sources.gentoo.org/" title="Browse our source code"><span class="fa fa-code fa-fw"></span>
                      Sources</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="https://infra-status.gentoo.org/" title="Get updates on the services provided by Gentoo"><span class="fa fa-server fa-fw"></span>
                      Infra Status</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <nav class="tyrian-navbar navbar navbar-dark navbar-expand-lg bg-primary" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-main-collapse" aria-controls="navbar-main-collapse" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
          </div>
          <div class="collapse navbar-collapse navbar-main-collapse" id="navbar-main-collapse">
            <ul class="navbar-nav mr-auto">
              
              <li class="nav-item" hidden><a id="kc-header-wrapper" class="nav-link text-capitalize">${kcSanitize(msg("loginTitleHtml",(realm.displayNameHtml!'')))?no_esc}</a></li>
              
	      <#if realm.displayNameHtml == '<div class="kc-logo-text"><span>Keycloak</span></div>'>
                <li class="nav-item"><a class="nav-link active" href="/auth/realms/gentoo/account/"><#nested "header"></a></li>
                <li class="nav-item active"><a class="nav-link" href="/auth/admin/">Admin</a></li>
	      <#else>
              <li class="nav-item active"><a class="nav-link active" href="/auth/realms/gentoo/account/"><#nested "header"></a></li>
              <li class="nav-item"><a class="nav-link" href="/auth/admin/">Admin</a></li>
	      </#if>
              <li class="nav-item"><a class="nav-link" href="https://wiki.gentoo.org/wiki/Project:Infrastructure/Single_Sign-on">Help</a></li>
            </ul>

          </div>
        </div>
      </nav>
    </header>

<style>
.kc-logo-text {
  display: inline-block;
}
</style>

   <div class="container">
      <div class="row">
        <div id="content" class="col-md-12 pb-5">

<h1 class="text-capitalize">${kcSanitize(msg("loginTitleHtml",(realm.displayNameHtml!'')))?no_esc} &ndash; Single Sign On</h1>

		<div class="d-flex justify-content-center mt-4 p-3">
<!--
		    <div class="row p-4" style="border: 1px solid lightgray; border-radius: 5px;">
-->
		    <div class="row p-4 mx-1" style="background: #EEEEEE; border-radius: 6px;">
		        <div class="col-md-6">



  <div class="${properties.kcLoginClass!}">
    <div id="kc-header" class="${properties.kcHeaderClass!}">
      <div id="kc-header-wrapper" class="${properties.kcHeaderWrapperClass!} d-none">${kcSanitize(msg("loginTitleHtml",(realm.displayNameHtml!'')))?no_esc}</div>
    </div>
    <div class="${properties.kcFormCardClass!} <#if displayWide>${properties.kcFormCardAccountClass!}</#if>">
      <header class="${properties.kcFormHeaderClass!}">
        <#if realm.internationalizationEnabled  && locale.supported?size gt 1>
            <div id="kc-locale">
                <div id="kc-locale-wrapper" class="${properties.kcLocaleWrapperClass!}">
                    <div class="kc-dropdown" id="kc-locale-dropdown">
                        <a href="#" id="kc-current-locale-link">${locale.current}</a>
                        <ul>
                            <#list locale.supported as l>
                                <li class="kc-dropdown-item"><a href="${l.url}">${l.label}</a></li>
                            </#list>
                        </ul>
                    </div>
                </div>
            </div>
        </#if>
        <#if !(auth?has_content && auth.showUsername() && !auth.showResetCredentials())>
            <#if displayRequiredFields>
                <div class="${properties.kcContentWrapperClass!} d-none">
                    <div class="${properties.kcLabelWrapperClass!} subtitle">
                        <span class="subtitle"><span class="required">*</span> ${msg("requiredFields")}</span>
                    </div>
                    <div class="col-md-10">
                        <h1 id="kc-page-title"><#nested "header"></h1>
                    </div>
                </div>
            <#else>
                <h1 id="kc-page-title" class="d-none"><#nested "header"></h1>
            </#if>
        <#else>
            <#if displayRequiredFields>
                <div class="${properties.kcContentWrapperClass!}">
                    <div class="${properties.kcLabelWrapperClass!} subtitle">
                        <span class="subtitle"><span class="required">*</span> ${msg("requiredFields")}</span>
                    </div>
                    <div class="col-md-10">
                        <#nested "show-username">
                        <div class="${properties.kcFormGroupClass!}">
                            <div id="kc-username">
                                <label id="kc-attempted-username">${auth.attemptedUsername}</label>
                                <a id="reset-login" href="${url.loginRestartFlowUrl}">
                                    <div class="kc-login-tooltip">
                                        <i class="${properties.kcResetFlowIcon!}"></i>
                                        <span class="kc-tooltip-text">${msg("restartLoginTooltip")}</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            <#else>
                <#nested "show-username">
                <div class="${properties.kcFormGroupClass!}">
                    <div id="kc-username">
                        <label id="kc-attempted-username">${auth.attemptedUsername}</label>
                        <a id="reset-login" href="${url.loginRestartFlowUrl}">
                            <div class="kc-login-tooltip">
                                <i class="${properties.kcResetFlowIcon!}"></i>
                                <span class="kc-tooltip-text">${msg("restartLoginTooltip")}</span>
                            </div>
                        </a>
                    </div>
                </div>
            </#if>
        </#if>
      </header>
      <div id="kc-content">
        <div id="kc-content-wrapper">

          <#-- App-initiated actions should not see warning messages about the need to complete the action -->
          <#-- during login.                                                                               -->
          <#if displayMessage && message?has_content && (message.type != 'warning' || !isAppInitiatedAction??)>
              <div class="alert alert-${message.type}">
                  <#if message.type = 'success'><span class="${properties.kcFeedbackSuccessIcon!}"></span></#if>
                  <#if message.type = 'warning'><span class="${properties.kcFeedbackWarningIcon!}"></span></#if>
                  <#if message.type = 'error'><span class="${properties.kcFeedbackErrorIcon!}"></span></#if>
                  <#if message.type = 'info'><span class="${properties.kcFeedbackInfoIcon!}"></span></#if>
                  <span class="kc-feedback-text">${kcSanitize(message.summary)?no_esc}</span>
              </div>
          </#if>

          <#nested "form">

          <#if auth?has_content && auth.showTryAnotherWayLink() && showAnotherWayIfPresent>
          <form id="kc-select-try-another-way-form" action="${url.loginAction}" method="post" <#if displayWide>class="${properties.kcContentWrapperClass!}"</#if>>
              <div <#if displayWide>class="${properties.kcFormSocialAccountContentClass!} ${properties.kcFormSocialAccountClass!}"</#if>>
                  <div class="${properties.kcFormGroupClass!}">
                    <input type="hidden" name="tryAnotherWay" value="on" />
                    <a href="#" id="try-another-way" onclick="document.forms['kc-select-try-another-way-form'].submit();return false;">${msg("doTryAnotherWay")}</a>
                  </div>
              </div>
          </form>
          </#if>

          <#if displayInfo>
              <div id="kc-info" class="${properties.kcSignUpClass!}">
                  <div id="kc-info-wrapper" class="${properties.kcInfoAreaWrapperClass!}">
                      <#nested "info">
                  </div>
              </div>
          </#if>
        </div>
      </div>

    </div>
  </div>


</div>

    <div class="col-md-6">
        <img src="https://www.gentoo.org/assets/img/bg/larry.png"/>
    </div>

</div>
</div>


<!--- tyrian -->


       </div>
      </div>
    </div>


    <footer>
      <div class="container">
        <div class="row">
          <div class="col-12 offset-md-2 col-md-7">
            &nbsp;
          </div>
          <div class="col-xs-12 col-md-3">
            <h3 class="footerhead">Questions or comments?</h3>
            Please feel free to <a href="https://www.gentoo.org/inside-gentoo/contact/">contact us</a>.
          </div>
        </div>
        <div class="row">
          <div class="col-3 col-md-2">
            <ul class="footerlinks three-icons">
              <li><a href="https://twitter.com/gentoo" title="@Gentoo on Twitter"><span class="fa fa-twitter fa-fw"></span></a></li>
              <li><a href="https://www.facebook.com/gentoo.org" title="Gentoo on Facebook"><span class="fa fa-facebook fa-fw"></span></a></li>
              <li></li>
            </ul>
          </div>
          <div class="col-xs-10 col-sm-9 col-md-10">
            <strong>&copy; 2001&ndash;2020 Gentoo Foundation, Inc.</strong><br>
            <small>
              Gentoo is a trademark of the Gentoo Foundation, Inc.
              The contents of this document, unless otherwise expressly stated, are licensed under the
              <a href="https://creativecommons.org/licenses/by-sa/4.0/" rel="license">CC-BY-SA-4.0</a> license.
              The <a href="https://www.gentoo.org/inside-gentoo/foundation/name-logo-guidelines.html">Gentoo Name and Logo Usage Guidelines</a> apply.
            </small>
          </div>
        </div>
      </div>
    </footer>

    <script src="https://assets.gentoo.org/tyrian/v2/jquery-3.3.slim.js"></script>
    <script src="https://assets.gentoo.org/tyrian/v2/popper.min.js"></script>
    <script src="https://assets.gentoo.org/tyrian/v2/bootstrap.min.js"></script>

</body>
</html>
</#macro>
